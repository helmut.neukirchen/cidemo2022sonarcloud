package is.hi.cs.junit5demo;


// Unit under test
public class Money {
	private int amount;

	
	// Constructor
	public Money(int amount) {
		this.amount = amount;
	}

	// Getter
	public int getAmount() {
		return amount;
	}

	// Add
	public Money add(Money m) {
		return new Money(amount + m.getAmount());
	}

	public Money subtract(Money m) {
		return new Money(amount - m.getAmount());
	}

	public Money multiply(int f) {
		return new Money(amount * f);
	}

	public Money divide(int d) {
		return new Money(amount / d);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Money) {
			return (amount == ((Money) o).getAmount());
		} else
			return false;
	}

	// Simple hash
	@Override
	public int hashCode() {
		return amount;
	}

	@Override
	public String toString() {
		return Integer.toString(amount);
	}
	
	// Just a comment
	
	public void prettyPrint() {
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("                   ");
		System.out.println(" Money =  "+toString());
		System.out.println("                   ");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");
		System.out.println("*******************");

	}
}