package is.hi.cs.junit5demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Tag("UsingHamcrest")
class MoneyTestWithAssertThat {

	private Money m12;
	private Money m14;

	@BeforeEach
	void constructMoneyTestObjects() {
		m12 = new Money(12);
		m14 = new Money(14);
	}

	// Test equals
	@Test
	public void testEqualsUsingFixture() {
		Money equalMoney = new Money(12);
		assertThat(m12, is(equalTo(m12)));
		assertThat(m12, is(equalTo(equalMoney)));
		assertThat(m12, is(not(equalTo(m14))));
	}

	@Test
	void testHashCode() {
		Money equalMoney = new Money(12);
		assertThat(m12.hashCode(), is(equalTo(equalMoney.hashCode())));
		assertThat(m12.hashCode(), is(equalTo(m12.hashCode())));
		assertThat(m12.hashCode(), is(not(equalTo(m14.hashCode()))));
	}
	
	// Test add
	@Test
	public void testAddUsingFixture() {
		Money expected = new Money(26);
		assertThat(m12.add(m14), is(equalTo(expected)));
	}
	
	// Test subtract
	@Test
	public void testSubtractUsingFixture() {
		Money expected = new Money(2);
		assertThat(m14.subtract(m12), is(equalTo(expected)));
	}
}
