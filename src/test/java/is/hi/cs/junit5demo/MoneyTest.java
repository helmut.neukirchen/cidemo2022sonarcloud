package is.hi.cs.junit5demo;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

// Test class without fixture

class MoneyTest {

	// Test equals
	@Test
	void testEquals() {
		Money m12 = new Money(12);
		Money m14 = new Money(14);
		Money equalMoney = new Money(12);
		assertTrue(m12.equals(m12));
		assertTrue(m12.equals(equalMoney));
		assertFalse(m12.equals(m14));
		assertFalse(m12.equals(null));
	}

	// Test add
	@Test
	void testAdd() {
		Money m12 = new Money(12);
		Money m14 = new Money(14);
		Money expected = new Money(26);
		assertEquals(expected, m12.add(m14));
	}

	// Test subtract
	@Test
	void testSubtract() {
		Money m12 = new Money(12);
		Money m14 = new Money(14);
		Money expected = new Money(2);
		assertEquals(expected, m14.subtract(m12));
	}
	
	@Test
	void testHashCode() {
		Money m12 = new Money(12);
		Money m14 = new Money(14);
		Money equalMoney = new Money(12);
		assertEquals(m12.hashCode(),equalMoney.hashCode());
		assertEquals(m12.hashCode(),m12.hashCode());
		assertNotEquals(m12.hashCode(),m14.hashCode());
	}
}