package is.hi.cs.junit5demo;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class MoneyTestWithFixture {
	private Money m12;
	private Money m14;

	@BeforeEach
	void constructMoneyTestObjects() {
		m12 = new Money(12);
		m14 = new Money(14);
	}

	
	// Test equals
	@Test
	public void testEquals() {
		Money equalMoney = new Money(12);
		assertTrue(m12.equals(m12));
		assertTrue(m12.equals(equalMoney));
		assertFalse(m12.equals(m14));
	}

	// Test add
	@Test
	public void testAdd() {
		Money expected = new Money(26);
		assertEquals(expected, m12.add(m14));
	}
	
	// Test subtract
	@Test
	public void testSubtract() {
		Money expected = new Money(2);
		assertEquals(expected, m14.subtract(m12));
	}
	
	@Test
	void testHashCode() {
		Money equalMoney = new Money(12);
		assertEquals(m12.hashCode(),equalMoney.hashCode());
		assertEquals(m12.hashCode(),m12.hashCode());
		assertNotEquals(m12.hashCode(),m14.hashCode());
	}
	
}
